<?php

/**
 * Response Class.
 */
class Response {
	private $code, $header, $body;

	public function __construct($server_output, $header_length, $response_code) {
		$this->code = $response_code;
		    
	    $this->header = $this->parse_header( substr($server_output, 0, $header_length) );

	    $this->body = substr($server_output, $header_length);
	}

	public function code($code = false) {
		return $code ? ($code == $this->code) : $this->code;
	}

	public function header($item = false) {
		return ($item && is_array($this->header)) ? $this->header[$item] : $this->header;
	}

	public function body($type = 'object') {
		switch ($type) {
			case 'object':
				return json_decode($this->body);
			case 'array':
				return json_decode($this->body, true);
			case 'string':
			default:
				return $this->body;
		}
	}

	private function parse_header($header) {
		$headers = [];

	    $headersText = substr($header, stripos($header, "\r\n"));
	    $headersText = explode("\r\n", $headersText);
	    foreach ($headersText as $key => $header) {
	        list($key, $value) = (strstr($header, ': ') ? explode(': ', $header) : [$header, '']);
	        if ($key == null) continue;
	        $headers[$key] = $value;
	    }

	    return $headers;
	}
}
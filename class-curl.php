<?php

require_once 'class-response.php';

/**
 * Curl Class.
 */
class Curl {
	private $request, $response, $url, $data, $headers;

	public function __construct($url, $data, $headers = '') {
        $this->url = $url;
		$this->data = $data;
		$this->headers = $headers;
    }

    private function exec($options, $headers) {
    	$this->request = curl_init();

    	curl_setopt_array($this->request, $options);

		if ( $headers ) {
			curl_setopt($this->request, CURLOPT_HTTPHEADER, $headers);
		}

    	$server_output = curl_exec($this->request);

		$header_length = curl_getinfo($this->request, CURLINFO_HEADER_SIZE);
		$response_code = curl_getinfo($this->request, CURLINFO_HTTP_CODE);

		curl_close($this->request);

		return new Response($server_output, $header_length, $response_code);
    }

	public function get() {
		$options = array(
			CURLOPT_URL => $this->url . $this->parse_data($this->data, true),
			CURLOPT_HEADER => true,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_HTTPGET => true
		);

		if ( $this->headers ) {
			$headers = ['Content-Type: application/json', $this->headers];
		} else $headers = ['Content-Type: application/json'];

		return $this->exec($options, $headers);
	}

	public function post() {
		$options = array(
			CURLOPT_URL => $this->url,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => $this->parse_data($this->data),
		    CURLOPT_HEADER => true,
		    CURLOPT_RETURNTRANSFER => true
		);

		$headers = '';
		if ( $this->headers ) {
			$headers = ['Content-Type: application/json', $this->headers];
		} else $headers = ['Content-Type: application/json'];

		return $this->exec($options, $headers);
	}

	public function put() {
		$data = $this->parse_data($this->data);

		$options = array(
			CURLOPT_URL => $this->url,
		    CURLOPT_CUSTOMREQUEST => 'PUT',
		    CURLOPT_POSTFIELDS => $data,
		    CURLOPT_HEADER => true,
		    CURLOPT_RETURNTRANSFER => true
		);

		if ( $this->headers ) {
			$headers = ['Content-Type: application/json', 'Content-Length: ' . strlen($data), $this->headers];
		} else $headers = ['Content-Type: application/json', 'Content-Length: ' . strlen($data)];

		return $this->exec($options, $headers);
	}

	public function delete() {
		$data = $this->parse_data($this->data);

		$options = array(
			CURLOPT_URL => $this->url,
		    CURLOPT_CUSTOMREQUEST => 'DELETE',
		    CURLOPT_POSTFIELDS => $data,
		    CURLOPT_HEADER => true,
		    CURLOPT_RETURNTRANSFER => true
		);

		if ( $this->headers ) {
			$headers = ['Content-Type: application/json', 'Content-Length: ' . strlen($data), $this->headers];
		} else $headers = ['Content-Type: application/json', 'Content-Length: ' . strlen($data)];

		return $this->exec($options, $headers);
	}

	private function parse_data($data, $stringify = false) {
		if ( is_null($data) || empty($data) ) return '';

		if ( is_array($data) && $stringify ) {
			$string = '';

			foreach ($data as $key => $value) {
				$prefix = ($string == '') ? '?' : '&';
				$string .= $prefix . urlencode($key) . '=' . urlencode($value);
			}

			return $string;
		} elseif ( is_array($data) || is_object($data) ) return json_encode($data);

		return $data;
	}
}